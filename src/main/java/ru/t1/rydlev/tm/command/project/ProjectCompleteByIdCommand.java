package ru.t1.rydlev.tm.command.project;

import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @Override
    public String getName() {
        return "project-complete-by-id";
    }

}

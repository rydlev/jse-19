package ru.t1.rydlev.tm.command.project;

import ru.t1.rydlev.tm.enumerated.Sort;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.util.DateUtil;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        int index = 1;
        for (final Project project : projects) {
            final String id = project.getId();
            final String name = project.getName();
            final String description = project.getDescription();
            final String status = Status.toName(project.getStatus());
            final String created = DateUtil.formatDate(project.getCreated());
            System.out.printf("%s. %s : %s : %s : %s; id: %s \n", index, name, status, created, description, id);
            index++;
        }
    }

    @Override
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public String getName() {
        return "project-list";
    }

}

package ru.t1.rydlev.tm.command.task;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        getTaskService().clear();
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

}

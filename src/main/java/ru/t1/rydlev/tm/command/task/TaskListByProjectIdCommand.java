package ru.t1.rydlev.tm.command.task;

import ru.t1.rydlev.tm.model.Task;
import ru.t1.rydlev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectID = TerminalUtil.nextLine();
        final String userId = getUserId();
        final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectID);
        renderTasks(tasks);
    }

    @Override
    public String getDescription() {
        return "Show task list by project id.";
    }

    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

}

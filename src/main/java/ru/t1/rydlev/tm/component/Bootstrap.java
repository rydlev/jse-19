package ru.t1.rydlev.tm.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.t1.rydlev.tm.api.component.IBootstrap;
import ru.t1.rydlev.tm.api.repository.ICommandRepository;
import ru.t1.rydlev.tm.api.repository.IProjectRepository;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.repository.IUserRepository;
import ru.t1.rydlev.tm.api.service.*;
import ru.t1.rydlev.tm.command.AbstractCommand;
import ru.t1.rydlev.tm.command.project.*;
import ru.t1.rydlev.tm.command.system.*;
import ru.t1.rydlev.tm.command.task.*;
import ru.t1.rydlev.tm.command.user.*;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.rydlev.tm.exception.system.CommandNotSupportedException;
import ru.t1.rydlev.tm.model.User;
import ru.t1.rydlev.tm.repository.CommandRepository;
import ru.t1.rydlev.tm.repository.ProjectRepository;
import ru.t1.rydlev.tm.repository.TaskRepository;
import ru.t1.rydlev.tm.repository.UserRepository;
import ru.t1.rydlev.tm.service.*;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class Bootstrap implements IBootstrap, IServiceLocator {

    private final static Logger LOGGER_LIFECYCLE = LoggerFactory.getLogger("LIFECYCLE");

    private final static Logger LOGGER_COMMANDS = LoggerFactory.getLogger("COMMANDS");

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserLockCommand());
        registry(new UserRegistryCommand());
        registry(new UserRemoveCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUnlockCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void initDemoData() {
        final User userTest = userService.create("test", "test", "test@test.ru");
        final String userTestId = userTest.getId();
        userService.create("user", "user", "user@user.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTestId, "DEMO PROJECT", "DEMO DESC");
        projectService.create(userTestId, "TEST PROJECT", "TEST DESC");
        projectService.create(userTestId, "EXAMPLE PROJECT", "EXAMPLE DESC");

        taskService.create(userTestId, "MEGA TASK", "MEGA DESC");
        taskService.create(userTestId, "VEGA TASK", "VEGA DESC");
        taskService.create(userTestId, "ALPHA TASK", "ALPHA DESC");
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public void run(final String... args) {
        parseArguments(args);
        initDemoData();
        initLogger();
        parseCommands();
    }

    private void initLogger() {
        LOGGER_LIFECYCLE.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                LOGGER_LIFECYCLE.info("*** TASK MANAGER IS SHUTTING DOWN ***");
            }
        });
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                LOGGER_COMMANDS.info(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                LOGGER_LIFECYCLE.error(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void parseCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

}
